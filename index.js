import express from "express";
import session from "express-session";
import passport from "passport";
import GoogleStrategy from "passport-google-oauth20";
import dotenv from "dotenv";

dotenv.config();

const ENV = {
  SESSION_SECRET: process.env.SESSION_SECRET,
  CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
  CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
  REDIRECT_URL: process.env.GOOGLE_REDIRECT_URL,
};

const app = express();

app.use(express.static("public"));
app.use(
  session({
    secret: ENV.CLIENT_SECRET,
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());

const googleStrategyConfig = {
  clientID: ENV.CLIENT_ID,
  clientSecret: ENV.CLIENT_SECRET,
  callbackURL: ENV.REDIRECT_URL,
};

passport.use(
  new GoogleStrategy(googleStrategyConfig, function (
    accessToken,
    refreshToken,
    params,
    profile,
    done
  ) {
    // console.log(params);
    // console.log(profile);
    console.log(params.id_token);
    done({ ...params, ...profile._json });
  })
);

app.get("/", (_, res) => {
  res.send("I am working!");
});

app.get(
  "/login/google",
  passport.authenticate("google", {
    scope: ["openid", "email", "profile"],
    accessType: "offline",
  })
);

app.get("/oauth2/redirect/google", (req, res, next) => {
  passport.authenticate("google", (user, err) => {
    if (err) {
      console.log(err);
      return res.redirect("/failure.html");
    }
    if (!user) return res.redirect("/index.html");
    return res.redirect("/success.html");
  })(req, res, next);
});

app.listen(3000, () => console.log("Server running at http://localhost:3000"));
