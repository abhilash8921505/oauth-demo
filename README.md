# OAuth Demo

## SignIn with Google

Used `passport` and `passport-google-oauth20` to simulate a sign-in with google flow

I created this app to get a Google OIDC identity token for testing another app.

I couldn't figure out a way to just get the OIDC id_token. So,I did the entire authentication flow i.e, getting OpenIDConnect Token & then getting access token, refresh token using the id_token.

### Learnings

- OAuth is an **Authorization** Protocol. OIDC is an **Authentication** protocol. Endpoints to get `id_token` in OIDC flow and endpoint for getting OAuth `authorization_token` are same in case of Google. It basically clubs them both. Google treats this as if "oidc" is another _scope_ of in its oauth endpoint.

- "oidc" scope is generally set by default. scope "profile" gives firstName, lastName. So I am asking for "oidc profile email".
